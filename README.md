Find It is a web-centered approach to helping residents find all their 
communities already have to offer.  It is especially focused on making sure
parents from all backgrounds and time availibility can easily find activities,
services, and resources for children.

## Background

In 2016-2017 Agaric built out [Find It Cambridge](https://finditcambridge.org)
under the direction of Nancy Tauber of the [Cambridge Kids' Council](http://www.cambridgema.gov/DHSP/programsforkidsandyouth/cambridgeyouthcouncil/familypolicycouncil)
and Dr. Leo Burd of MIT's Center for Civic Media, following hundreds of hours of
research aided by [Code for Boston](http://www.codeforboston.org/).

Read more [about Find It Cambridge](http://www.finditcambridge.org/about) and [the Find It platform](https://agaric.coop/findit).

## Using Find It

The Find It approach needs a person in government with dedicated time to champion the project,
to bring government agencies and program and service-providing
not-for-profits on board.

With that caveat that the software doesn't do everything, it is a big help!  It's
open source free software but the repository hosted by [Terravoz](https://github.com/terravoz/findit/) is currently private, as some sensitive information needs to be cleaned up.  It will also require some code customizations to work for your city, as some aspects of Cambride's branding and unique functionality is built into the current version of the software.  However we plan on upgrading the software so it can serve as a platform for any city or town that wants it.

[Agaric](http://agaric.com) is available to implement and support it, should you choose.  Find It Cambridge asked Agaric to take the lead
role in maintaining the site and an active role in promoting the approach to
other cities.  Find It is powered by the same underlying software, [Drupal](https://www.drupal.org), that is running mass.gov and
boston.gov.

## Pilot program

We would love to connect with the right people in city and town governments to
talk about spreading the Find It approach.

If we can secure commitments from ten local governments we will be able to
update, improve, and genericize (make suitable for a broader array of communities) the Find It software and support it efficiently as the Find It platform by Agaric.

Please [ask Agaric](http://agaric.com/contact) about Find It for your city.