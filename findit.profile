<?php
/**
 * @file
 * Provides configuration and content for a home page.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\findit\Form\RegisterForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_node_insert().
 *
 * Sets system.site.page.front to the home page node's path.
 *
 * We cannot do it in configuration because the content is created after the
 * configuration, and without the content the configuration import will fail.
 *
 * And we cannot set the front page to a path alias, which does allow the
 * configuration to import, because that breaks Drupal's front page detection.
 *
 * See https://gitlab.com/drutopia/drutopia/issues/225
 */
function findit_node_insert(EntityInterface $node) {
  $uuid = $node->uuid();
  if ($uuid === '17924c78-7c7b-4c81-8aef-902605dcc880') {
    \Drupal::configFactory()
      ->getEditable('system.site')
      ->set('page.front', '/node/' . $node->id())
      ->save();
  }
}

/**
 * Consolidate all the profiles into one.
 *
 * The idea of this function is:
 *  - Get rid of all the individual profiles
 *  - If the user has an organization profile use it if not create it.
 *  - Copy all the values of the individual profiles into this org profile.
 *    The organization values has precedence over the individual profiles, in
 *    the case of a user with multiple organization profiles the profile with
 *    more references to nodes have precedence.
 *  - Update all the places where the old contact was referenced.
 *
 *  Some users have several organization/individual profiles, most of them are
 *  empty and caused by a bug in the migration, so they will be delete and just
 *  preserve those that have info under the following rules:
 *
 *  - If the profile is not referenced anywhere is safe to just delete it.
 *  - If the profile is used then check what of the profiles of the user has
 *    more references and use the most popular merging the data of the others
 *    into this one.
 *
 * @param int $user_id
 *   The profile that is going to be merged into the main profile.
 */
function findit_merge_profiles($user_id) {

  /** @var \Drupal\user\UserInterface $user */
  $user = \Drupal::entityTypeManager()->getStorage('user')->load($user_id);

  // Lets use the most used organization profile as main profile.
  $profile = findit_get_main_profile($user);

  // Get all the Individual profiles that are going to be merged.
  $secondary_profiles = \Drupal::entityQuery('profile')
    ->condition('uid', $user->id())
    ->condition('profile_id', $profile->id(), '<>')
    ->condition('type', 'crm_indiv')
    ->accessCheck(FALSE)
    ->execute();

  foreach ($secondary_profiles as $secondary_profile) {
    // Get all the nodes where this profile appears as a contact.
    $nodes = \Drupal::entityQuery('node')
      ->condition('field_findit_contacts', $secondary_profile)
      ->execute();

    /** @var \Drupal\profile\Entity\ProfileInterface $old_profile */
    $old_profile = \Drupal::entityTypeManager()->getStorage('profile')->load($secondary_profile);
    // Update the node references to the new profile and delete the old one.
    findit_update_profile_references($nodes,  $profile, $old_profile);
    findit_merge_profile($profile, $old_profile);
    $old_profile->delete();
  }
  $profile->save();
}

/**
 * Merge the old_profile values into the new_profile.
 *
 * @param \Drupal\profile\Entity\ProfileInterface $new_profile
 * @param \Drupal\profile\Entity\ProfileInterface $old_profile
 */
function findit_merge_profile(&$new_profile, $old_profile) {
  $name = $old_profile->get('crm_name')->getValue();
  if (!empty($name[0])) {
    $name = $name[0]['given'] . ' ' . $name[0]['family'];
  }
  else {
    $name = '';
  }
  $old_values = [
    'crm_org_name' => $name,
    'field_findit_crm_role' => $old_profile->get('field_findit_crm_role')->getString(),
    'field_public_email' => $old_profile->get('field_public_email')->getString(),
    'crm_phone' => $old_profile->get('crm_phone')->getString(),
    'field_findit_crm_tty_number' => $old_profile->get('field_findit_crm_tty_number')->getString(),
    'field_findit_crm_ext' => $old_profile->get('field_findit_crm_ext')->getString(),
    'crm_created_by' => $old_profile->get('crm_created_by')->getString(),
  ];
  foreach ($old_values as $field_name => $old_value) {
    $old_field_name = ($field_name == 'crm_org_name') ? 'crm_name' : $field_name;
    if (!empty($old_profile->get($old_field_name)->getString())) {
      $new_profile->set($field_name, $old_value);
    }
  }
}

/**
 * Update all the nodes, replace the old profile references by the new one.
 *
 * @param array $nodes
 *  An array of node ids that have a reference to the old profile.
 * @param $new_profile
 *  The new profile that is going to be used instead of the old one.
 * @param $old_profile
 *  The old profile that is going to be removed.
 */
function findit_update_profile_references(array $nodes, $new_profile, $old_profile) {
  if (empty($nodes)) {
    return TRUE;
  }
  foreach ($nodes as $nid) {
    $node = \Drupal::entityTypeManager()->getStorage('node')->load($nid);
    $field_value = $node->get('field_findit_contacts')->getValue();
    foreach ($field_value as $index => $value) {
      if ($value['target_id'] == $old_profile->id()) {
        $field_value[$index]['target_id'] = $new_profile->id();
      }
    }
    $node->set('field_findit_contacts', $field_value);
    $node->save();
  }
  return TRUE;
}

/**
 * Create a new profile that is going to replace all the "individual" profiles.
 * @param \Drupal\user\UserInterface $user
 *   User object.
 * @return \Drupal\profile\Entity\ProfileInterface $profile
 *   The org profile that is most referenced.
 */
function findit_get_main_profile($user) {
  // Create profile
  $main_profile = \Drupal::entityTypeManager()->getStorage('profile')->create([
    'type' => 'crm_org',
    'status' => 1,
    'is_default' => 1,
  ]);
  $main_profile->setOwner($user);
  $main_profile->save();
  return $main_profile;
}

/**
 * Implements hook_entity_type_build().
 *
 * Add contacts dashboard form operation to profile entity.
 */
function findit_entity_type_build(array &$entity_types) {
  $entity_types['user']->setFormClass('register', RegisterForm::class);
}

/**
 * Implements hook_form_alter.
 */
function findit_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ($form_id == 'views_exposed_form' && $form['#id'] == 'views-exposed-form-contacts-dashboard-indexed-full') {
    array_unshift($form['#submit'], '_findit_lowercase_contacts_search');
  }
}

/**
 * The `ignore case`  preprocess query is somehow ignored in the contacts
 * view, so we will change the values manually.
 * @param array $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 */
function _findit_lowercase_contacts_search(array &$form, FormStateInterface $form_state) {
  $form_state->setValue('search', strtolower($form_state->getValue('search')));
}
